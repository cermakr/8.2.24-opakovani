#include <DHT.h>
#include <ErriezSerialTerminal.h>

#define pinDHT 5
#define typDHT11 DHT11

DHT mojeDHT(pinDHT, typDHT11);

char newlineChar = '\n'; 
char delimiterChar = ' ';
int cislo1 = 0;
int cislo2 = 0;
int index  = 0;
int vysledek = 0;
SerialTerminal term(newlineChar, delimiterChar);

float hmd = 0;

int x=0;
float pole[10];



void setup()
{

  Serial.begin(115200);
  mojeDHT.begin();
  
  

  term.addCommand("nazdarek", pozdrav);
  term.addCommand("cislo", zadejcislo);
  term.addCommand("kolik_je_stupnu", teplota);
  term.addCommand("jaka_je_vlhkost", vlhkost);
  term.addCommand("jake_je_pocasi", pocasi);
  term.addCommand("historie_teploty", histerieTeploty);
}

void histerieTeploty()
{
for (int i=0;i<10;i++){
    Serial.print (pole[i]);
    Serial.print (" ");
  }
}

void pocasi()
{ Serial.println("podivej se z okna");
}
void vlhkost()

{
  hmd = mojeDHT.readHumidity();
  Serial.println(hmd);
}
void teplota()
{
  float tep = mojeDHT.readTemperature();
  if (x+1 <10){
    pole[x] = tep;
    Serial.println(tep);
  }
  else{
    x = 0;
    pole[x] = tep;
    Serial.println(tep);
  }
  x += 1;
}

void pozdrav()
{
  Serial.println(F("zdravicko"));
}

void loop(){
  
  term.readSerial();
  
  
  
}

void zadejcislo(){
  char *arg;
  arg = term.getNext();
  if (arg != NULL) {
    index += 1;
      if ((index%2) == 1){
        cislo1 = atoi(arg);
        Serial.println(cislo1 );
      }
      else {
        cislo2 = atoi(arg);
        Serial.println(cislo2 );
      }
    Serial.print(F("Argument: "));
    Serial.println(arg);
    vysledek = ((cislo1) + (cislo2));
    Serial.print("vysledek je " );
    Serial.println(vysledek );
  } else {
    Serial.println(F("No argument"));
}
}
